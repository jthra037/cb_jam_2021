﻿public interface IHealth
{
	float HP { get; }
	void ChangeHP(float delta);
	void SetHP(float newHP);
}
