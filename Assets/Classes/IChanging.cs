﻿public delegate void ChangeDelegate<T>(T change, T newAmount, T maxAmount);

public interface IChanging<T>
{
	event ChangeDelegate<T> ChangeEvent;
}
