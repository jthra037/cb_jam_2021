﻿using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

public class MovementActions : MonoBehaviour
{
	[SerializeField]
	private float speed;

	private PlayerController playerController;

	private Vector2 movement;

	private void Start()
	{
		playerController = GetComponent<PlayerController>();
	}

	private void Update()
	{
		move(movement);
	}

	public void MoveEvent(CallbackContext context)
	{
		movement = context.ReadValue<Vector2>();
	}

	private void move(Vector2 movement)
	{
		playerController.Move(movement.normalized * speed);
	}
}
