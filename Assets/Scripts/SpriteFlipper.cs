﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(Rigidbody2D))]
public class SpriteFlipper : MonoBehaviour
{
	private SpriteRenderer spriteRenderer;
	private Rigidbody2D rb;

	// Start is called before the first frame update
	void Start()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		rb = GetComponent<Rigidbody2D>();
	}

	private void Update()
	{
		if (rb.velocity.x > 0)
		{
			spriteRenderer.flipX = false;
		}
		else if (rb.velocity.x < 0)
		{
			spriteRenderer.flipX = true;
		}
	}
}
