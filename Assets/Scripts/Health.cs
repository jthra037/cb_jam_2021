﻿using NSJT.ComponentHub;
using UnityEngine;

[RequireComponent(typeof(IKillable))]
public class Health : MonoBehaviour, IHealth, IChanging<float>
{
	public event ChangeDelegate<float> ChangeEvent;

	private IKillable owner;

	[SerializeField]
	private bool iframes;

	[SerializeField]
	private float hp;

	[SerializeField]
	private float maxHP;
	public float MaxHP
	{
		get
		{
			return maxHP;
		}
		set
		{
			maxHP = value;
		}
	}

	public float HP
	{
		get
		{
			return hp;
		}
		private set
		{
			float before = hp;
			hp = value;
			ChangeEvent?.Invoke(hp - before, hp, MaxHP);
		}
	}

	private void OnEnable()
	{
		ChangeEvent += handleZeroHP;
	}

	private void handleZeroHP(float change, float newTotal, float maxAmount)
	{
		if (newTotal <= 0)
		{
			owner.Kill();
		}
	}

	private void Awake()
	{
		Hub.Register<Health>(this);
	}

	private void OnDestroy()
	{
		Hub.Unregister<Health>(this);
	}

	private void Start()
	{
		owner = GetComponent<IKillable>();
	}

	public void ChangeHP(float delta)
	{
		if (!iframes)
			HP += delta;
	}

	public void SetHP(float newHP)
	{
		HP = newHP;
	}
}
