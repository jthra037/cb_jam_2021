﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class RigidbodyMover : MonoBehaviour
{
	private Rigidbody2D rigidbody;
	[SerializeField]
	private float speed;
	[SerializeField]
	private float tolerance;

	private bool shouldMove;
	private Vector3 lastPlayerLocation;

	private Vector3 movementDir => (lastPlayerLocation - transform.position).normalized;

	// Start is called before the first frame update
	void Start()
	{
		rigidbody = GetComponent<Rigidbody2D>();
	}

	private void FixedUpdate()
	{
		if (shouldMove)
		{
			rigidbody.velocity = movementDir * speed;
		}
	}

	private void Update()
	{
		if ((lastPlayerLocation - transform.position).sqrMagnitude < (tolerance * tolerance))
		{
			shouldMove = false;
			rigidbody.velocity = Vector3.zero;
		}
	}

	public void GoTo(Vector3 position)
	{
		shouldMove = true;
		lastPlayerLocation = position;
	}
}
