﻿using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

[RequireComponent(typeof(PlayerController))]
public class AttackActions : MonoBehaviour
{
	private PlayerController playerController;

	private void Start()
	{
		playerController = GetComponent<PlayerController>();
	}

	public void Attack(CallbackContext context)
	{
		if (context.phase == InputActionPhase.Started)
		{
			playerController.Attack();
		}
	}
}
