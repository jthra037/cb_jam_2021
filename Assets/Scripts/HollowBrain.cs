﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(Rigidbody2D))]
public class HollowBrain : MonoBehaviour, IKillable
{
	private const string speed = "Speed";
	private const string death = "Death";
	private const string attack = "Attack";

	private Animator animator;
	private Rigidbody2D rigidbody;

	private bool isDead = false;
	[SerializeField]
	private float timeToDespawn;
	[SerializeField]
	private float timeBetweenAttacks;
	private float timeToAttack;

	private void Start()
	{
		animator = GetComponent<Animator>();
		rigidbody = GetComponent<Rigidbody2D>();
	}

	private void Update()
	{
		animator.SetFloat(speed, rigidbody.velocity.magnitude);

		if (Time.time > timeToAttack
			&& Vector3.Distance(PlayerController.Instance.transform.position, transform.position) < 2)
		{
			timeToAttack = Time.time + timeBetweenAttacks;
			animator.SetTrigger(attack);
		}
	}

	public void Kill()
	{
		animator.SetTrigger(death);
		isDead = true;
		StartCoroutine(waitForTimeThenDestroy());
	}

	private IEnumerator waitForTimeThenDestroy()
	{
		yield return new WaitForSeconds(timeToDespawn);

		Destroy(gameObject);
	}
}
