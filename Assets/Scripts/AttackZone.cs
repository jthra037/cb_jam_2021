﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class AttackZone : MonoBehaviour
{
	[SerializeField]
	private GundryBrain gundryBrain;

	[SerializeField]
	private string zoneName;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Player"))
		{
			gundryBrain.PlayerEnteredHitZone(zoneName);
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.CompareTag("Player"))
		{
			gundryBrain.PlayerLeftHitZone(zoneName);
		}
	}
}
