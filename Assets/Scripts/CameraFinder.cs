﻿using UnityEngine;
using UnityEngine.InputSystem;

public class CameraFinder : MonoBehaviour
{
	private PlayerInput playerInput;

	private Camera camera;

	private void OnEnable()
	{
		camera = Camera.current;
		playerInput = GetComponent<PlayerInput>();
	}

	private void attachCamera()
	{
		playerInput.camera = camera;
	}

	public Camera GetCamera()
	{
		return camera;
	}
}
