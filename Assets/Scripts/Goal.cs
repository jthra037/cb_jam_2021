﻿using UnityEngine;

[RequireComponent(typeof(SceneLoader))]
public class Goal : MonoBehaviour
{
	private SceneLoader loader;

	private void Start()
	{
		loader = GetComponent<SceneLoader>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Player"))
		{
			loader.LoadScene();
		}
	}
}
