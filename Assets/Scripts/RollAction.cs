﻿using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

[RequireComponent(typeof(PlayerController))]
public class RollAction : MonoBehaviour
{
	private PlayerController playerController;

	private void Start()
	{
		playerController = GetComponent<PlayerController>();
	}

	public void Roll(CallbackContext context)
	{
		if (context.phase == InputActionPhase.Started)
		{
			playerController.Roll();
		}
	}
}
