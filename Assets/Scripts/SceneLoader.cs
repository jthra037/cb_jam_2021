﻿using UnityEngine;
using UnityEngine.SceneManagement;
using static UnityEngine.InputSystem.InputAction;

public class SceneLoader : MonoBehaviour
{
	[SerializeField]
	private string sceneName;

	public void LoadScene(CallbackContext context)
	{
		if (context.performed)
		{
			SceneManager.LoadScene(sceneName);
		}
	}

	public void LoadScene()
	{
		SceneManager.LoadScene(sceneName);
	}

	public void LoadScene(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}
}
