﻿using NSJT.ComponentHub;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Stamina))]
public class PlayerController : MonoBehaviour, IKillable
{
	private static PlayerController instance;
	public static PlayerController Instance
	{
		get
		{
			return instance;
		}
		private set
		{
			if (instance != null)
			{
				Destroy(instance.gameObject);
			}
			instance = value;
		}
	}

	private Rigidbody2D rb;

	private Vector2 velocity;

	public delegate void StaminaDelegate(float cost);
	public event StaminaDelegate AttackEvent;
	public event StaminaDelegate RollEvent;


	public delegate void MoveDelegate(Vector2 velocity);
	public event MoveDelegate MoveEvent;

	public delegate void DeathDelegate();
	public event DeathDelegate DeathEvent;

	[SerializeField]
	private float attackCost;
	[SerializeField]
	private float rollCost;


	private Stamina stamina;

	private bool isRolling;

	private Coroutine rollRoutineRef;
	[SerializeField]
	private AnimationCurve rollSpeedCurve;
	[SerializeField]
	private AnimationClip rollClip;

	private void Awake()
	{
		Instance = this;
		Hub.Register(this);
	}

	private void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		stamina = GetComponent<Stamina>();
	}

	private void FixedUpdate()
	{
		rb.velocity = velocity;
	}

	private void Update()
	{
		MoveEvent?.Invoke(rb.velocity);
	}

	public void Move(Vector2 newVelocity)
	{
		if (!isRolling)
			velocity = newVelocity;
	}

	public void Attack()
	{
		if (stamina.Current > attackCost)
			AttackEvent?.Invoke(-attackCost);
	}

	public void Roll()
	{
		if (rollRoutineRef == null
			&& stamina.Current > rollCost)
		{
			rollRoutineRef = StartCoroutine(rollRoutine());
			RollEvent?.Invoke(-rollCost);
		}
	}

	public void Kill()
	{
		DeathEvent?.Invoke();
	}

	private void OnDestroy()
	{
		if (Instance == transform)
		{
			Instance = null;
		}

		Hub.Unregister(this);
	}

	private IEnumerator rollRoutine()
	{
		isRolling = true;
		float duration = rollClip.length;
		float progress = duration;
		Vector2 startVelocity = velocity;

		while (progress > 0)
		{
			velocity = startVelocity * rollSpeedCurve.Evaluate(1f - progress / duration);
			yield return null;
			progress -= Time.deltaTime;
		}

		velocity = startVelocity;
		isRolling = false;
		rollRoutineRef = null;
	}
}
