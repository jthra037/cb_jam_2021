﻿using NSJT.ComponentHub;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarFillUpdater : MonoBehaviour
{
	public enum BarValueType
	{
		Health,
		Mana,
		Stamina
	}
	[SerializeField]
	private BarValueType BarType;

	[SerializeField]
	private Image image;

	private IChanging<float> value;
	private Action unGetValue;

	private void OnEnable()
	{
		getValueByType(BarType);
		registerCallbacks();
	}

	private void OnDisable()
	{
		unregisterCallbacks();
		unGetValue?.Invoke();
		unGetValue = null;
	}

	private void getValueByType(BarValueType t)
	{
		switch(t)
		{
			case BarValueType.Stamina:
				getValue<Stamina>();
				break;
			case BarValueType.Mana:
				// doesn't exist yet
				break;
			default:
				getValue<Health>();
				break;
		}
	}

	private void getValue<T>() where T: Component, IChanging<float>
	{
		int handle = Hub.Get<T>(setValue, value => value.CompareTag("Player"));
		unGetValue = () => Hub.UnGet<T>(handle);
	}

	private void setValue<T>(T obj) where T:Component, IChanging<float>
	{
		value = obj;
		registerCallbacks();
	}

	private void registerCallbacks()
	{
		if (value != null)
			value.ChangeEvent += onHealthChange;
	}

	private void onHealthChange(float change, float newTotal, float maxAmount)
	{
		image.fillAmount = newTotal / maxAmount;
	}

	private void unregisterCallbacks()
	{
		if (value != null)
			value.ChangeEvent -= onHealthChange;
	}
}
