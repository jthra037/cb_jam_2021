﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBoolOnStateExit : StateMachineBehaviour
{
	[SerializeField]
	private string name;
	[SerializeField]
	private bool value;

	public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animator.SetBool(name, value);
	}
}
