﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(Rigidbody2D))]
public class GundryBrain : MonoBehaviour, IKillable
{
	private const string speed = "Speed";
	private const string death = "Death";
	private const string attack = "Attack";

	private Animator animator;
	private Rigidbody2D rigidbody;

	private bool isDead = false;
	[SerializeField]
	private float timeToDespawn;
	[SerializeField]
	private float timeBetweenAttacks;
	private float timeToAttack;

	private List<string> playerAreas = new List<string>(2);

	private void Start()
	{
		animator = GetComponent<Animator>();
		rigidbody = GetComponent<Rigidbody2D>();
	}

	private void Update()
	{
		animator.SetFloat(speed, rigidbody.velocity.magnitude);

		if (Time.time > timeToAttack
			&& playerAreas.Count > 0)
		{
			timeToAttack = Time.time + timeBetweenAttacks;
			string zone = playerAreas[Random.Range(0, playerAreas.Count)];
			animator.SetTrigger(zone);
		}
	}

	public void PlayerEnteredHitZone(string zoneName)
	{
		playerAreas.Add(zoneName);
	}

	public void PlayerLeftHitZone(string zoneName)
	{
		playerAreas.Remove(zoneName);
	}

	public void Kill()
	{
		animator.SetTrigger(death);
		isDead = true;
		StartCoroutine(waitForTimeThenDestroy());
	}

	private IEnumerator waitForTimeThenDestroy()
	{
		yield return new WaitForSeconds(timeToDespawn);

		Destroy(gameObject);
	}
}
