﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(PlayerController), typeof(Stamina))]
public class PlayerAnimations : MonoBehaviour
{
	private Animator animator;
	private PlayerController playerController;

	private const string attack = "Attacking";
	private const string speed = "Speed";
	private const string death = "Death";
	private const string roll = "Roll";

	private void OnEnable()
	{
		registerCallbacks();
	}

	private void OnDisable()
	{
		unregisterCallbacks();
	}

	// Start is called before the first frame update
	void Start()
	{
		animator = GetComponent<Animator>();
		playerController = GetComponent<PlayerController>();

		registerCallbacks();
	}

	private void registerCallbacks()
	{
		if (playerController)
		{
			playerController.AttackEvent += onAttack;
			playerController.RollEvent += onRoll;
			playerController.MoveEvent += onMove;
			playerController.DeathEvent += onDeath;
		}
	}

	private void onRoll(float _)
	{
		animator.SetTrigger(roll);
	}

	private void unregisterCallbacks()
	{
		if (playerController)
		{
			playerController.AttackEvent -= onAttack;
			playerController.RollEvent -= onRoll;
			playerController.MoveEvent -= onMove;
			playerController.DeathEvent -= onDeath;
		}
	}

	private void onAttack(float _)
	{
		animator.SetBool(attack, true);
	}

	private void onMove(Vector2 velocity)
	{
		animator.SetFloat(speed, velocity.magnitude);
	}

	private void onDeath()
	{
		animator.SetTrigger(death);
		unregisterCallbacks();
	}
}
