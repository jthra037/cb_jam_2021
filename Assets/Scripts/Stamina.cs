﻿using NSJT.ComponentHub;
using System;
using UnityEngine;

public class Stamina : MonoBehaviour, IChanging<float>
{
	public event ChangeDelegate<float> ChangeEvent;

	[SerializeField]
	private float current;

	[SerializeField]
	private float max;
	public float MaxStamina
	{
		get
		{
			return max;
		}
		set
		{
			max = value;
		}
	}

	[SerializeField]
	private float regenRate;
	private Action unGetAction;

	public float Current
	{
		get
		{
			return current;
		}
		private set
		{
			float before = current;
			current = Mathf.Clamp(value, 0, max);
			ChangeEvent?.Invoke(current - before, current, max);
		}
	}

	private void OnEnable()
	{
		if (PlayerController.Instance)
			PlayerController.Instance.AttackEvent += Change;
			PlayerController.Instance.RollEvent += Change;
	}

	private void OnDisable()
	{
		PlayerController.Instance.AttackEvent -= Change;
		PlayerController.Instance.RollEvent -= Change;
	}

	private void Awake()
	{
		Hub.Register(this);
	}

	private void Start()
	{
		int handle = Hub.Get<PlayerController>(registerAttackCallback);
		unGetAction = () => Hub.UnGet<PlayerController>(handle);
	}

	private void registerAttackCallback(PlayerController obj)
	{
		obj.AttackEvent += Change;
		obj.RollEvent += Change;
		unGetAction?.Invoke();
		unGetAction = null;
	}

	private void OnDestroy()
	{
		Hub.Unregister(this);
	}

	private void Update()
	{
		Change(regenRate * Time.deltaTime);
	}

	public void Change(float delta)
	{
		Current += delta;
	}

	public void Set(float newValue)
	{
		current = newValue;
	}
}
