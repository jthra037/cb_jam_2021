﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Weapon : MonoBehaviour
{
	[SerializeField]
	private float damage;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		IHealth health = collision.gameObject.GetComponent<IHealth>();
		health?.ChangeHP(-damage);
	}
}
