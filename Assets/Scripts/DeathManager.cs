﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


[RequireComponent(typeof(PlayerInput), typeof(PlayerController))]
public class DeathManager : MonoBehaviour
{
	private PlayerInput input;
	private PlayerController controller;
	private SceneLoader sceneloader;

	[SerializeField]
	private float timeToWait = 3f;

	[SerializeField]
	private string postDeathMapName;

	private void OnEnable()
	{
		if (controller)
		{
			registerListeners();
		}
	}

	private void OnDisable()
	{
		unregisterListeners();
	}

	private void Start()
	{
		input = GetComponent<PlayerInput>();
		controller = GetComponent<PlayerController>();
		sceneloader = FindObjectOfType<SceneLoader>();

		registerListeners();
	}

	private void registerListeners()
	{
		controller.DeathEvent += onPlayerDeath;
	}

	private void onPlayerDeath()
	{
		StartCoroutine(waitForSecondsThenChangeScene(timeToWait));
	}

	private void unregisterListeners()
	{
		controller.DeathEvent -= onPlayerDeath;
	}

	private IEnumerator waitForSecondsThenChangeScene(float timeToWait)
	{
		yield return new WaitForSeconds(timeToWait);

		sceneloader.LoadScene();
	}
}
