﻿using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class PlayerFinder : MonoBehaviour
{
	private Transform target;
	[SerializeField]
	private Transform origin;
	[SerializeField]
	private RigidbodyMover mover;
	[SerializeField]
	private float sightDistance;
	[SerializeField]
	private ContactFilter2D contactFilter;

	private bool inRange;

	private int layerMask;

	private void Start()
	{
		GetComponent<CircleCollider2D>().isTrigger = true;
		layerMask = LayerMask.GetMask("Player", "Walls");
		target = PlayerController.Instance.transform;
	}

	private void FixedUpdate()
	{
		if (inRange)
		{
			RaycastHit2D hit = Physics2D.Raycast(origin.position, target.position - origin.position, sightDistance, layerMask);
			if (hit.transform == target.transform)
			{
				mover.GoTo(hit.point);
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.transform.Equals(target))
		{
			inRange = true;
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.transform.Equals(target))
		{
			inRange = false;
		}
	}
}
